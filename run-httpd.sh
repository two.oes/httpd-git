#!/bin/bash

echo "
<VirtualHost *:8080>
   SetEnv GIT_PROJECT_ROOT /data/
   SetEnv GIT_HTTP_EXPORT_ALL
   DocumentRoot /data/
   ScriptAlias / /usr/libexec/git-core/git-http-backend/
   <Directory "/usr/libexec/git-core">
      Options +ExecCGI -MultiViews +SymLinksIfOwnerMatch
      AllowOverride None
      Require all granted
   </Directory>
   <Directory "/data/">
      Dav On
      Options Indexes FollowSymLinks
      AllowOverride None
      Require all granted
   </Directory>
</VirtualHost>
" > /tmp/git-server.conf
mv /tmp/git-server.conf /opt/app-root/git-server.conf

/usr/sbin/httpd $OPTIONS -DFOREGROUND
